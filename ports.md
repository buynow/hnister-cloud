## 组件端口

| 服务名称 | 功能  | 端口  |
| :---:   | :-: | :-: |
| hnister-config-server | 服务配置中心 | 8888 |
| hnister-eureka-server | 服务注册中心 | 8889 |
| hnister-zuul-server | 服务代理端口 | 10000 |
| hnister-news-service | 新闻服务 | 9001 |
| hnister-security-service | 安全服务 | 9002 |
| hnister-schoolmate-service | 校友服务 | 9003 |
